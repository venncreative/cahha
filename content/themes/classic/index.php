<?php get_header(); ?>

        <?php if(get_field('slideshow')) : ?>
            <section class="slideshow-container">
                <div class="slideshow">
                    <div class="slides">
                        <?php the_post_thumbnail('slideshow', array('class' => 'full-width')); ?>
                        <?php if($slides = get_field('slideshow')) : foreach($slides as $slide) : ?>
                            <img src="<?php echo $slide['sizes']['slideshow']; ?>" alt="<?php echo $slide['alt']; ?>" class="full-width slide">
                        <?php endforeach; endif; ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 center">
                        <div class="slideshow-pager"></div>
                    </div>
                </div>

            </section>
        <?php endif; ?>
        
    <section class="section">
        <div class="row">
            <div class="col-8 tablet-col-12">
                <div class="content">
                    <?php if(have_posts()) : while(have_posts()) : the_post(); ?>

                        <h1><?php the_title(); ?></h1>

                        <?php if(get_field('standfirst')) : ?>
                            <p class="standfirst">
                                <?php the_field('standfirst'); ?>
                            </p>
                        <?php endif; ?>

                        <?php the_content(); ?>
                        
                    <?php endwhile; else : get_404_template(); endif; ?>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>
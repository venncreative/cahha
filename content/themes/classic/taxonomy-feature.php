<?php get_header(); ?>

        <section class="section">
            <div class="row">
                <div class="col-12">
                    <h1>Properties for "<?php single_term_title(); ?>"</h1>
                </div>

                <div class="col-3 tablet-col-12">
                    <?php get_template_part('parts/property-sidebar'); ?>
                </div>

                <div class="container-9 cf">
                    <div class="col-12 right" style="position: relative; top: -30px; margin-bottom: -30px;">
                        <a href="#" class="js-view-switcher" data-view="property-list">List View</a> /
                        <a href="#" class="js-view-switcher" data-view="property-grid">Grid View</a>
                    </div>


                    <?php if(have_posts()) : while(have_posts()) : the_post(); ?>

                        <?php get_template_part('parts/property'); ?>

                    <?php endwhile; else : get_404_template(); endif; ?>

                </div>

                <div class="col-9 push-3">
                    <?php
                        // print_r($wp_query);
                    ?>
                    <?php get_template_part('parts/pagination'); ?>
                </div>
            </div>
        </section>


        <?php get_footer(); ?>
    </body>
</html>
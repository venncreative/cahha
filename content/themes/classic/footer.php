<footer class="section main-footer block no-margin">
    <div class="row">
        <div class="col-6">


            <?php

                $menu = array(
                    'theme_location'  => 'footer-menu',
                    'menu'            => '',
                    'container'       => 'false',
                    'container_class' => '',
                    'container_id'    => '',
                    'menu_class'      => 'nav footer-menu',
                    'menu_id'         => 'footer-menu',
                    'fallback_cb'     => false,
                );

                wp_nav_menu($menu);

            ?>
            <p class="copyright"><?php the_field('footer_copyright', 'options'); ?><br>Site by <a href="http://venncreative.co.uk" target="_blank">Venn Creative</a></p>
        </div>
        <div class="col-6">
            <ul class="nav social_profiles">
                <li>
                    <a href="https://www.facebook.com/pages/Towan-Valley/554485851287892">
                        <img src="<?php bloginfo('template_directory'); ?>/img/social_facebook.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="https://twitter.com/towan_valley">
                        <img src="<?php bloginfo('template_directory'); ?>/img/social_twitter.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="https://www.pinterest.com/towanvalley/">
                        <img src="<?php bloginfo('template_directory'); ?>/img/social_pinterest.png" alt="">
                    </a>
                </li>
            </ul>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />

        <!-- SEO Meta -->
        <title></title>

        <!-- For Responsive Layouts -->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1.0" />

        <?php if(get_field('google_webmaster_code', 'options')) : the_field('google_webmaster_code', 'options'); endif; ?>

        <!-- CSS -->
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/reset.css">
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/grid.css">
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/style.css">
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/responsive.css">

        <!-- <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/theme.css"> -->

        <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style.css">

        <!-- Custom Styling -->
        <style><?php get_template_part('parts/theme'); ?></style>


        <?php if(get_field('site_font', 'options') && get_field('site_font', 'options') !== "Arial") : ?>
            <link href='https://fonts.googleapis.com/css?family=<?php the_field('site_font', 'options') ?>' rel='stylesheet' type='text/css'>
            <style>
                html,
                body {
                    font-family: "<?php echo str_replace('+', ' ', get_field('site_font', 'options')); ?>";
                }
            </style>

        <?php endif; ?>
        <!-- JS -->
        <script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/html5.min.js"></script>

        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/cycle.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/site.js"></script>

        <?php wp_head(); ?>

        <?php if(get_field('google_analytics_code', 'options')) : the_field('google_analytics_code', 'options'); endif; ?>
    </head>

    <body <?php body_class(); ?>><?php if(get_field('google_tag_manager_code', 'options')) : the_field('google_tag_manager_code', 'options'); endif; ?>

        <header class="section main-header block">
            <div class="row cf">
                <div class="col-3 no-margin">
                    <a href="<?php echo site_url('/'); ?>">
                        <?php if($logo = get_field('site_logo', 'options')) : ?>
                            <img src="<?php echo $logo; ?>" class="main-logo" alt="<?php bloginfo('name'); ?>">
                        <?php else : ?>
                            <?php bloginfo('name'); ?>
                        <?php endif; ?>
                        <!-- <img src="<?php bloginfo('template_directory'); ?>/img/logo.jpg" width="180" class="main-logo" alt="Logo"> -->
                    </a>
                </div>

                <div class="col-9 no-margin">

                    <?php if(get_field('header_text', 'options')) : ?>
                        <div class="header-contact">
                            <?php the_field('header_text', 'options'); ?>
                        </div>
                    <?php endif; ?>

                    <?php

                        $menu = array(
                            'theme_location'  => 'main-menu',
                            'menu'            => '',
                            'container'       => 'false',
                            'container_class' => '',
                            'container_id'    => '',
                            'menu_class'      => 'nav main-menu',
                            'menu_id'         => 'main-menu',
                            'fallback_cb'     => false,
                        );

                        wp_nav_menu($menu);

                    ?>
                </div>
            </div>
        </header>
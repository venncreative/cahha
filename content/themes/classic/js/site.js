var resizeImg = function() {

    $('.slides img').each(function() {

        var height = $(this).height();

        var sHeight = $('.slides').height();

        if(height > sHeight) {
            $(this).css({
                "margin-top" : "-" + (height - sHeight) / 2 + "px"
            });
        }
    });


};

$(window).load(function() {
    resizeImg();
});

$(document).ready(function() {

    resizeImg();

    // resize/center image on window resize/orientation change
    $(window).on('resize', resizeImg);

    // slideshow
    $('.slides').cycle({
        slides : ".slide",
        pager : ".slideshow-pager",
        pagerTemplate : "<div class='pager-button'></div>",
        autoHeight : "calc"
    });




    $('.js-tab').click(function(event) {

        event.preventDefault();

        var target = $(this).attr('href');

        if(!$(target).hasClass('is-visible')) {

            $('.js-tab-content.is-visible').fadeOut(200, function() {

                $(this).removeClass('is-visible').addClass('is-hidden');

                $(target).fadeIn(200, function() {
                    $(this).removeClass('is-hidden').addClass('is-visible');
                });

            });
        }

    });


    $('.js-view-switcher').click(function() {

        var view = $(this).data('view');

        if(view == "property-list") {
            $('.property-archive').removeClass('property-grid').addClass(view);
        } else {
            $('.property-archive').removeClass('property-list').addClass(view);
        }
    });



    $('.js-overlay').hover(function() {
        $(this).children('.overlay').animate({
            "padding-bottom" : "30px"
        }, 200);
    }, function() {
        $(this).children('.overlay').animate({
            "padding-bottom" : "10px"
        }, 200);
    });



});
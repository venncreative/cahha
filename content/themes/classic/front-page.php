<?php get_header(); ?>

    <?php if($slides = get_field('slideshow')) : ?>

        <section class="slideshow-container">
            <div class="slideshow">
                <div class="slides">

                    <?php foreach($slides as $slide) : ?>
                        <img src="<?php echo $slide['sizes']['slideshow']; ?>" alt="<?php echo $slide['alt']; ?>" class="full-width slide">
                    <?php endforeach; ?>

                </div>
            </div>

            <div class="row">
                <div class="col-12 center">
                    <div class="slideshow-pager"></div>
                </div>
            </div>
        </section>

    <?php endif; ?>

        <section class="section">
            <div class="row center">
                <div class="col-8 tablet-col-12 push-2">

                    <?php if(get_field('standfirst')) : ?>
                        <h1 class="standfirst">
                            <?php the_field('standfirst'); ?>
                        </h1>
                    <?php endif; ?>

                    <?php if(have_posts()) : while(have_posts()) : the_post(); ?>

                        <?php the_content(); ?>

                    <?php endwhile; else : get_404_template(); endif; ?>

                    <?php if(get_field('button_text') && get_field('button_link')) : ?>
                        <a href="<?php the_field('button_link'); ?>" class="btn btn-large"><?php the_field('button_text'); ?></a>
                    <?php endif; ?>

                </div>

            </div>
        </section>

        <?php if($featured = get_field('featured_content')) : // Featured Properties Area ?>

        <section class="section">
            <div class="row">
                <div class="col-12">
                    <h5 class="section-title">Featured Properties</h5>
                </div>

                <?php foreach($featured as $post) : setup_postdata($post); ?>
                    <div class="col-6 tablet-col-6">

                        <div class="featured-overlay js-overlay" onclick="window.location='<?php the_permalink(); ?>'">
                            <?php get_the_image(array('meta_key' => 'medium', 'size' => 'medium', 'image_class' => 'featured-image')); ?>
                            <div class="overlay">
                                <h4 class="overlay-title">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php the_title(); ?>
                                    </a>
                                </h4>
                                <?php if(get_field('property_details')) : ?>
                                    <p class="overlay-text"><?php the_field('property_details'); ?></p>
                                <?php endif; ?>
                            </div>
                        </div>

                    </div>
                <?php endforeach; wp_reset_postdata(); endif; ?>
            </div>
        </section>

        <?php if(get_field('featured_sub_content') == "content" && $content = get_field('custom_content')) : ?>
            <section class="section">
                <div class="row">
                    <div class="col-12">
                        <h5 class="section-title">Featured Content</h5>
                    </div>


                <?php $i = 0; foreach($content as $post) : setup_postdata($post); ?>
                    <?php if($i === 2) { echo '<div class="cf tablet-show"></div>'; } ?>
                    <div class="col-3 tablet-col-6 mobile-col-6">

                        <?php get_the_image(array( 'meta_key' => 'thumbnail', 'size' => 'thumbnail', 'image_class' => 'full-width')); ?>

                        <h4><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>

                        <div class="milli">
                            <?php if(get_field('standfirst')) : ?>
                                <?php the_field('standfirst'); ?>
                            <?php else : ?>
                                <?php the_excerpt(); ?>
                            <?php endif; ?>
                        </div>
                        <br>
                        <a href="<?php the_permalink(); ?>" title="Read more: <?php the_title(); ?>">Read More</a>
                    </div>
                <?php wp_reset_postdata(); $i++; endforeach; ?>
                </div>
            </section>

        <?php elseif(get_field('featured_sub_content') == "blog") : ?>
            <?php $q = new WP_Query('post_type=post&posts_per_page=4'); ?>

            <?php if($q->have_posts()) : ?>
                <section class="section">
                    <div class="row">
                        <div class="col-12">
                            <h5 class="section-title">From the Blog</h5>
                        </div>
                        <?php $i = 0; while($q->have_posts()) : $q->the_post(); ?>
                        <?php if($i === 2) { echo '<div class="cf tablet-show"></div>'; } ?>

                        <div class="col-3 tablet-col-6 mobile-col-6">

                            <?php get_the_image(array( 'meta_key' => 'thumbnail', 'size' => 'thumbnail', 'image_class' => 'full-width')); ?>

                            <h4><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>

                            <div class="milli">
                                <?php if(get_field('standfirst')) : ?>
                                    <?php the_field('standfirst'); ?>
                                <?php else : ?>
                                    <?php the_excerpt(); ?>
                                <?php endif; ?>
                            </div>
                            <br>
                            <a href="<?php the_permalink(); ?>" title="Read more: <?php the_title(); ?>">Read More</a>
                        </div>
                        <?php $i++; endwhile; ?>
                    </div>
                </section>
            <?php endif; ?>

        <?php endif; // endif(get_field('featured_sub_content') == "blog") ?>


        <?php get_footer(); ?>
    </body>
</html>
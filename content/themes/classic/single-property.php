<?php get_header(); ?>
    <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
        <?php if(get_field('slideshow')) : ?>
            <section class="slideshow-container">
                <div class="slideshow">
                    <div class="slides">
                        <!-- <div class="slide"> -->
                            <?php if($slides = get_field('slideshow')) : foreach($slides as $slide) : ?>
                                <img src="<?php echo $slide['sizes']['slideshow']; ?>" alt="<?php echo $slide['alt']; ?>" class="full-width slide">
                            <?php endforeach; endif; ?>
                        <!-- </div> -->

                        <div class="slide-overlay">
            <?php endif; ?>

                            <div class="row">
                                <div class="col-8 no-margin">
                                    <h1 class="slide-title"><?php the_title(); ?></h1>
                                    <?php if(get_field('standfirst')) : ?>
                                        <p><?php the_field('standfirst'); ?></p>
                                    <?php endif; ?>
                                </div>
                            </div>

             <?php if(get_field('slideshow')) : ?>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-12 center">
                        <div class="slideshow-pager"></div>
                    </div>
                </div>

            </section>
        <?php endif; ?>


        <section class="section">
            <div class="row">
                <div class="col-2">
                    <ul class="nav-stacked tabs property-tabs">
                        <li><a href="#tab-1" class="js-tab">Overview</a></li>

                        <?php if(get_field('property_tabs')) : $i = 2; while(has_sub_field('property_tabs')) : ?>
                            <li>
                                <a href="#tab-<?php echo $i; ?>" class="js-tab">
                                    <?php the_sub_field('tab_title'); ?>
                                </a>
                            </li>
                        <?php $i++; endwhile; endif; ?>

                    </ul>
                </div>
                <div class="col-7">
                    <div id="tab-1" class="js-tab-content is-visible">
                        <h2>Overview</h2>

                            <?php the_content(); ?>

                    </div>
                    <?php if(get_field('property_tabs')) : $i = 2; while(has_sub_field('property_tabs')) : ?>
                        <div id="tab-<?php echo $i; ?>" class="js-tab-content is-hidden">
                            <h2><?php the_sub_field('tab_title'); ?></h2>

                            <?php the_sub_field('tab_content'); ?>

                        </div>
                    <?php $i++; endwhile; endif; ?>

                </div>

                <div class="col-3">
                    <?php if(get_field('property_booking_link')) : ?>
                        <a href="<?php the_field('property_booking_link'); ?>" target="_blank" class="btn btn-booking">
                            <span class="gamma" style="color: #fff; font-weight: normal;">Book Now</span><br> on Aspects Holidays
                        </a>
                    <?php endif; ?>

                    <div class="sidebar">                      
                        
                        <?php if(get_field('property_details')) : ?>
                            <div class="widget">
                                <!-- <h4 class="widget-title">Details</h4> -->
                                <p><?php the_field('property_details'); ?>
                            </div>
                        <?php endif; ?>

                        <?php if(get_field('property_price_details')) : ?>
                        <div class="widget">
                            <h4 class="widget-title">Price</h4>
                            <p><?php the_field('property_price_details'); ?></p>
                        </div>
                        <?php endif; ?>

                        <div class="widget">
                            <h4 class="widget-title">Features</h4>
                            <?php echo get_the_term_list($post->ID, 'feature', '<ul><li>', '</li><li>', '</li></ul>'); ?>
                        </div>

                    </div>
                </div>

                <div class="col-12">
                    <h3>Check Availability</h3>
                    <iframe style="width: 100%; height: 300px; overflow: hidden;" title="Aspects Holidays Availability" src="http://www.aspects-holidays.co.uk/feeds/availabilityWidget.aspx?cottagecode=<?php the_field('property_code'); ?>" frameborder="0" scrolling="no"></iframe>
                </div>
            </div>
        </section>
        <?php endwhile; else : get_404_template(); endif; ?>
        <?php get_footer(); ?>

    </body>
</html>
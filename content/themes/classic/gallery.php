<?php get_header(); ?>

    <section class="section">
        <div class="row">
            <div class="col-12">
                <h1>Gallery Page</h1>
            </div>


            <?php for($i = 0; $i < 12; $i++) : ?>
                <div class="col-3 tablet-col-4 mobile-col-6">
                    <img src="http://placehold.it/220x220&text=Gallery Image" class="full-width" />
                </div>
            <?php endfor; ?>
        </div>
    </section>


<?php get_footer(); ?>
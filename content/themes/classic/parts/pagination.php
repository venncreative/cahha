<?php

    global $wp_query; /* this is the general loop */
    global $q; /* this is our custom WP_Query */

    if(!isset($q)) : $q = $wp_query; endif;

    // print_r($wp_query);

    if($q->query_vars['paged']) : $current = $q->query_vars['paged']; else : $current = 1; endif;

    $args = array(
        'current' => $current,
        'total' => $q->max_num_pages,
        'base' => get_pagenum_link(1) . '%_%',
        'format' => 'page/%#%',
        'type' => 'list',
        'prev_text'    => __('&larr;'),
        'next_text'    => __('&rarr;')
    );

    echo paginate_links($args);
?>
<div class="property property-archive property-grid">

    <a href="<?php the_permalink(); ?>">
        <?php // the_post_thumbnail('thumb_square', array('class' => 'property-image')); ?>
        <?php get_the_image(array('size' => 'thumb_square', 'image_class' => 'property-image')); ?>
    </a>

    <div class="property-info">
        <h4 class="property-title">
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                <?php the_title(); ?>
            </a>
        </h4>
        <p class="milli" style="color: #888;">
            <?php if(get_field('property_details')) : ?>
                <?php the_field('property_details'); ?><br>
            <?php endif; ?>
            
            <?php if(get_field('property_price')) : ?>
                From: &pound;<?php the_field('property_price'); ?>
            <?php endif; ?>
        </p>

        <div class="property-details">

            <p style="font-size: 0.875em;">
                <?php if(get_field('standfirst')) : ?>
                    <?php the_field('standfirst'); ?>
                <?php else : ?>
                    <?php the_excerpt(); ?>
                <?php endif; ?>
            </p>

            <a href="<?php the_permalink(); ?>" class="btn">View Property</a>
        </div>
    </div>
</div>
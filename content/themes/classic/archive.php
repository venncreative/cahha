<?php get_header(); ?>

        <section class="section">
            <div class="row">
                <div class="col-12">
                    <h1>
                        Posts for
                        <?php if(is_category()) : ?>
                           "<?php single_cat_title(); ?>"
                        <?php elseif(is_tag()) : ?>
                            "<?php single_tag_title(); ?>"
                        <?php elseif(is_date()) : ?>
                           <?php single_month_title(' '); ?>
                        <?php endif; ?>
                    </h1>

                </div>
                <div class="col-8">

                    <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
                    <div class="post post-archive">
                        <?php the_post_thumbnail('thumb_square', array('class' => 'post-image')); ?>

                        <h3 class="post-title">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_title(); ?>
                            </a>
                        </h3>

                        <p class="post-date milli"><?php the_time(get_option('date_format')); ?></p>

                        <?php the_excerpt(); ?>

                        <a href="<?php the_permalink(); ?>" class="btn">Read More</a>
                    </div>
                <?php endwhile; else : get_404_template(); endif; ?>

                    <div>
                        <?php get_template_part('parts/pagination'); ?>
                    </div>
                </div>


                <div class="col-3 push-1">
                    <?php get_sidebar(); ?>
                </div>

            </div>
        </section>


        <?php get_footer(); ?>
    </body>
</html>
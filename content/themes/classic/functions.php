<?php

// Set up theme
add_action('after_setup_theme', 'theme_setup' );

function theme_setup() {

    // Set up theme support
    if(function_exists('add_theme_support')) {

        // Add Thumbnail Support
        add_theme_support('post-thumbnails');

        // additional Image Sizes
        if(function_exists('add_image_size')) {
            // add_image_size('name', width, height, crop);

            add_image_size('slideshow', 1000, 500, true);
            add_image_size('medium', 460, 220, true);
            add_image_size('featured_square', 460, 460, true);
            add_image_size('thumbnail', 220, 160, true);
            add_image_size('thumb_square', 220, 220, true);

        }

        // Add Menus
        add_theme_support('nav-menus');
        add_action('init', 'register_theme_menus');

        // Register Theme Menus
        function register_theme_menus() {

            register_nav_menus(
                array(
                    'main-menu' => __('Main Menu'),
                    'footer-menu' => __('Footer Menu')
                )
            );

        }


        // Register Sidebars
        if(function_exists('register_sidebar')) {

            // Sidebar Properties
            $sidebar = array(
                'name' => __('Blog Sidebar'),
                'id' => 'blog-sidebar',
                'before_widget' => '<div class="widget">',
                'after_widget' => '</div>',
                'before_title' => '<h4 class="widget-title">',
                'after_title' => '</h4>',
            );

            // Register Sidebar
            register_sidebar($sidebar);

        }

    }

}


function custom_excerpt_length( $length ) {
    return 20;
}

add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


function new_excerpt_more( $more ) {
    return '&hellip;';
}

add_filter('excerpt_more', 'new_excerpt_more');



// include custom post type class
include_once('cpt/cpt.php');

// create properties post type
$names = array(
    'post_type_name' => 'property',
    'singular' => 'Property',
    'plural' => 'Properties',
    'slug' => 'property'
);

$properties = new CPT($names, array('menu_position' => 5));


// register feature taxonomy
$properties->register_taxonomy('feature', array('rewrite' => array('slug' => 'property/feature')));

$properties->sortable(array(
    'feature' => array('feature')
));

// change icon for post
$properties->menu_icon('dashicons-admin-home');


// $properties->columns(array(
//     'cb' => '<input type="checkbox" />',
//     'title' => __('Title'),
//     'from_price' => __('From Price'),
//     'feature' => __('Features'),
//     'date' => __('Date')
// ));


// $properties->populate_column('from_price', function($post_id, $column_name) {

//     echo "£" . get_field('property_price');

// });


// $properties->sortable(array(
//     'from_price' => array('property_price', true)
// ));





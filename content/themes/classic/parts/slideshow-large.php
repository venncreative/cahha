<section class="slideshow-container">
    <div class="slideshow">
        <div class="slides">
            <div class="slide">
                <img src="<?php bloginfo('template_directory'); ?>/img/290004.jpg" alt="slideshow">
            </div>

            <div class="slide" width="100%">
                <img src="<?php bloginfo('template_directory'); ?>/img/290004.jpg" alt="slideshow" class="full-width">
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-12 center">
            <div class="slideshow-pager"></div>
        </div>
    </div>

</section>
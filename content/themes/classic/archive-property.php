<?php
/*
    Template Name: Property Listing
*/
?>
<?php get_header(); ?>

        <section class="section">
            <div class="row">
                <div class="col-12">
                    <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
                        <h1><?php the_title(); ?></h1>

                        <?php the_content(); ?>

                    <?php endwhile; else : get_404_template(); endif; ?>
                </div>

                <div class="col-3 tablet-col-12">
                    <?php get_template_part('parts/property-sidebar'); ?>
                </div>

                <div class="container-9 cf">
                    <div class="col-12 right" style="position: relative; top: -30px; margin-bottom: -30px;">
                        <a href="#" class="js-view-switcher" data-view="property-list">List View</a> /
                        <a href="#" class="js-view-switcher" data-view="property-grid">Grid View</a>
                    </div>


                    <?php
                        $args = array(
                            'post_type' => 'property',
                            'posts_per_page' => 12,
                            'paged' => get_query_var('paged')
                        );

                        $q = new WP_Query($args);
                    ?>

                    <?php if($q->have_posts()) : while($q->have_posts()) : $q->the_post(); ?>

                        <?php get_template_part('parts/property'); ?>

                    <?php endwhile; else : get_404_template(); endif; ?>

                </div>

                <div class="col-9 push-3">
                    <?php get_template_part('parts/pagination'); ?>
                </div>
            </div>
        </section>


        <?php get_footer(); ?>
    </body>
</html>
<?php get_header(); ?>

    <?php if(have_posts()) : while(have_posts()) : the_post(); ?>

        <?php if(get_field('slideshow')) : ?>
            <section class="slideshow-container">
                <div class="slideshow">
                    <div class="slides">
                        <?php get_the_image(array('size' => 'slideshow', 'image_class' => 'full-width slide')); ?>
                        <?php if($slides = get_field('slideshow')) : foreach($slides as $slide) : ?>
                            <img src="<?php echo $slide['sizes']['slideshow']; ?>" alt="<?php echo $slide['alt']; ?>" class="full-width slide">
                        <?php endforeach; endif; ?>
                        
                        <div class="slide-overlay">
                            <div class="row">
                                <div class="col-8 no-margin">
                                    <h1 class="slide-title"><?php the_title(); ?></h1>
                                    <p class="post-date"><?php the_time(get_option('date_format')); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
  

                <div class="row">
                    <div class="col-12 center">
                        <div class="slideshow-pager"></div>
                    </div>
                </div>
          <?php endif; ?>
        </section>

        <section class="section">
            <div class="row">
                <div class="col-8">

                    <div class="post post-single">
                        <?php if(get_field('standfirst')) : ?>
                            <p class="standfirst">
                                <?php the_field('standfirst'); ?>
                            </p>
                        <?php endif; ?>

                        <?php the_content(); ?>
                    </div>

                    <div id="comments">
                        <?php comments_template(); ?>
                    </div>
                </div>

        <?php endwhile; else : get_404_template(); endif; ?>

                <div class="col-3 push-1">
                    <?php get_sidebar(); ?>
                </div>

            </div>
        </section>


        <?php get_footer(); ?>
    </body>
</html>
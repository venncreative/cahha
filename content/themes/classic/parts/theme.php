<?php if(get_field('main_background_colour', 'options') || get_field('main_text_colour', 'options')) : ?>
    
    html,
    body {
        <?php if(get_field('main_background_colour', 'options')) : ?>
        background: <?php the_field('main_background_colour', 'options'); ?>;
        <?php endif; ?>
        <?php if(get_field('main_text_colour', 'options')) : ?>
        color: <?php the_field('main_text_colour', 'options'); ?>;
        <?php endif; ?>
    }

<?php endif; if(get_field('header_background_colour', 'options') || get_field('header_text_colour', 'options')) : ?>

    .main-header {
        <?php if(get_field('header_background_colour', 'options')) : ?>
        background: <?php the_field('header_background_colour', 'options'); ?>;
        <?php endif; ?>
        <?php if(get_field('header_text_colour', 'options')) : ?>
        color: <?php the_field('header_text_colour', 'options'); ?>;
        <?php endif; ?>
    }

<?php endif; if(get_field('footer_background_colour', 'options') || get_field('footer_text_colour', 'options')) : ?>

    .main-footer {
        <?php if(get_field('footer_background_colour', 'options')) : ?>
        background: <?php the_field('footer_background_colour', 'options'); ?>;
        <?php endif; ?>
        <?php if(get_field('footer_text_colour', 'options')) : ?>
        color: <?php the_field('footer_text_colour', 'options'); ?>;
        <?php endif; ?>
    }

<?php endif; if(get_field('link_colour', 'options')) : ?>

    a,
    a:visited {
        color: <?php the_field('link_colour', 'options'); ?>;
    }

    .main-menu .sub-menu li a:hover {
        background-color: #fff;
        color: <?php the_field('link_colour', 'options'); ?>;
    }

    .btn,
    .btn:visited,
    .comment-reply-link,
    #submit,
    .main-menu li a:hover,
    .main-menu li:hover,
    .main-menu li:hover a {
        background-color: <?php the_field('link_colour', 'options'); ?>;
        color: #fff;
        -webkit-transition: all .2s ease;
    }

    .pager-button {
        background-color: <?php the_field('link_colour', 'options'); ?>;
    }

<?php endif; if(get_field('link_hover_colour', 'options')) : ?>

    a:hover {
        color: <?php the_field('link_hover_colour', 'options'); ?>;
    }

    .btn:hover,
    .comment-reply-link:hover,
    #submit:hover {
        background-color: <?php the_field('link_hover_colour', 'options'); ?>;
        color: #fff;
    }

    .pager-button:hover,
    .cycle-pager-active {
        background-color: <?php the_field('link_hover_colour', 'options'); ?>;
    }

<?php endif; ?>